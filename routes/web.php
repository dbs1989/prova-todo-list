<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

// Authentication routes.
Auth::routes();

Route::get('/tasks', 'TasksController@index')->name('home');
Route::post('/tasks', 'TasksController@store');
Route::delete('/tasks/{task}', 'TasksController@destroy');
Route::patch('/tasks/{task}', 'TasksController@toggleDoneStatus');
//Route::get('/', 'WelcomeController@index');

//Route::resource('/categoria','CategoriaController');

//rotas categoria
//Route::get('/categorias', 'CategoriaController@index')->name('home');
//Route::post('/categorias', 'CategoriaController@store');
//Route::delete('/categorias/{categorias}', 'CategoriaController@destroy');


Route::resource('/categorias', 'CategoriaController');
