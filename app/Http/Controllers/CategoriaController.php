<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//se App\Repositories\CategoriaRepository;
use App\Categoria;

class CategoriaController extends Controller
{

	public function index()
	{

        $categorias = Categoria::all();

        /*return response()->json([
            'categorias' => $categorias
        ], 200);
*/
		return view('categorias.index', [
			'categorias' => $categorias
		]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|max:255',
        ]);

        $categoria = Categoria::create([
            'nome' => request('nome')
        ]);

		return back();

    }
    
    public function destroy(Request $request, Categoria $categoria)
	{

		$categoria->delete();

		return back();
	}
}
