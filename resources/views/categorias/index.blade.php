@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			<!-- New task form -->
			<form action="{{ url('categorias') }}" method="POST">
				{{ csrf_field() }}

				<div class="form-group">
					<div class="input-group">
						<!-- New task name -->
						<label for="newCategoryName" class="sr-only">Nova Categoria</label>
						<input type="text" name="nome" id="newCategoryName" class="form-control" placeholder="Nome">
						
						<!-- Add task button -->
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit">Adicionar</button>
						</span>
					</div>
				</div>

				<!-- Display validation errors -->
				@include('commons.errors')
			</form>			

			
			<!-- Categorias -->
			<div class="panel panel-info">
				<div class="panel-heading">Categorias</div>

				<div class="panel-body">
					@if(count($categorias))
						<table class="table table-hover" id="taskListTable">
							<thead>
								<tr>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($categorias as $categoria)
									<tr>
										<!-- Task name -->
										<td>
											{{ $categoria->nome }}
										</td>
										<td class="text-center">									
											<!-- Delete button -->
											<form class="inline" action="{{ url('categorias/'.$categoria->id) }}" method="POST">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}
												
												<button class="btn btn-default" type="submit" aria-label="Deletar" title="Deletar">
													<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
												</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
						<p class="text-center">
							<span class="glyphicon glyphicon-ice-lolly-tasted" aria-hidden="true"></span> Nenhum categoria adicionada ainda!;
						</p>
					@endif

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
